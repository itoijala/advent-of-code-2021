use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::combinator::all_consuming;
use nom::combinator::value;
use nom::multi::many1;
use nom::sequence::separated_pair;
use nom::sequence::terminated;
use nom::Finish;
use nom::IResult;

#[derive(Clone)]
enum Direction {
    Forward,
    Down,
    Up,
}

struct Movement {
    direction: Direction,
    distance: i64,
}

fn calculate_1(input: &[Movement]) -> i64 {
    let mut h = 0;
    let mut d = 0;
    for movement in input {
        match movement.direction {
            Direction::Forward => h += movement.distance,
            Direction::Down => d += movement.distance,
            Direction::Up => d -= movement.distance,
        }
    }
    h * d
}

fn calculate_2(input: &[Movement]) -> i64 {
    let mut aim = 0;
    let mut h = 0;
    let mut d = 0;
    for movement in input {
        match movement.direction {
            Direction::Forward => {
                h += movement.distance;
                d += aim * movement.distance;
            }
            Direction::Down => aim += movement.distance,
            Direction::Up => aim -= movement.distance,
        }
    }
    h * d
}

fn read_input() -> anyhow::Result<Vec<Movement>> {
    Ok(all_consuming(parse_input)(include_str!("../input/2.txt"))
        .finish()?
        .1)
}

fn parse_input(input: &str) -> IResult<&str, Vec<Movement>> {
    many1(terminated(parse_line, tag("\n")))(input)
}

fn parse_line(input: &str) -> IResult<&str, Movement> {
    let (input, (direction, distance)) = separated_pair(
        alt((
            value(Direction::Forward, tag("forward")),
            value(Direction::Down, tag("down")),
            value(Direction::Up, tag("up")),
        )),
        tag(" "),
        nom::character::complete::i64,
    )(input)?;
    Ok((
        input,
        Movement {
            direction: direction,
            distance: distance,
        },
    ))
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;

    let result_1 = calculate_1(&input);
    println!("{}", result_1);

    let result_2 = calculate_2(&input);
    println!("{}", result_2);

    Ok(())
}

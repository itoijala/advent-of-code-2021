use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::all_consuming;
use nom::combinator::map;
use nom::multi::many1;
use nom::sequence::terminated;
use nom::Finish;
use nom::IResult;

const BITS: u64 = 12;

fn calculate_1(input: &[u64]) -> u64 {
    let mut gamma = 0;
    for i in 0..(BITS - 1) {
        let mut ones = 0;
        for n in input {
            if n & (1 << i) != 0 {
                ones += 1;
            }
        }
        if ones >= input.len() / 2 {
            gamma |= 1 << i;
        }
    }
    gamma * (!gamma & 0xFFF)
}

fn calculate_2(input: &[u64]) -> u64 {
    let oxygen = {
        let mut input = input.to_vec();
        let mut i = BITS;
        while input.len() > 1 {
            i -= 1;
            let mut ones = 0;
            for n in &input {
                if n & (1 << i) == 1 << i {
                    ones += 1;
                }
            }
            let common = if 2 * ones >= input.len() { 1 } else { 0 };
            let mut left = Vec::new();
            for n in input {
                if n & (1 << i) == common << i {
                    left.push(n);
                }
            }
            input = left;
        }
        input[0]
    };
    let co2 = {
        let mut input = input.to_vec();
        let mut i = BITS;
        while input.len() > 1 {
            i -= 1;
            let mut ones = 0;
            for n in &input {
                if n & (1 << i) == 1 << i {
                    ones += 1;
                }
            }
            let common = if 2 * ones < input.len() { 1 } else { 0 };
            let mut left = Vec::new();
            for n in input {
                if n & (1 << i) == common << i {
                    left.push(n);
                }
            }
            input = left;
        }
        input[0]
    };
    oxygen * co2
}

fn read_input() -> anyhow::Result<Vec<u64>> {
    Ok(all_consuming(parse_input)(include_str!("../input/3.txt"))
        .finish()?
        .1)
}

fn parse_input(input: &str) -> IResult<&str, Vec<u64>> {
    many1(terminated(
        map(digit1, |s| u64::from_str_radix(s, 2).unwrap()),
        tag("\n"),
    ))(input)
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;

    let result_1 = calculate_1(&input);
    println!("{}", result_1);

    let result_2 = calculate_2(&input);
    println!("{}", result_2);

    Ok(())
}

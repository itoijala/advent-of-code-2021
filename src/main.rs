use anyhow::bail;

mod day_1;
mod day_2;
mod day_3;

fn main() -> anyhow::Result<()> {
    let args: Vec<String> = std::env::args().collect();
    match args[1].as_str() {
        "1" => day_1::run(),
        "2" => day_2::run(),
        "3" => day_3::run(),
        _ => bail!("Unknown day: {}", args[1]),
    }
}

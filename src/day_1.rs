use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

pub fn calculate_1(input: &[usize]) -> usize {
    let mut sum = 0;
    for window in input.windows(2) {
        if window[1] > window[0] {
            sum += 1;
        }
    }
    sum
}

pub fn calculate_2(input: &[usize]) -> usize {
    let mut sum = 0;
    let mut previous = None;
    for w in input.windows(3) {
        let current = w[0] + w[1] + w[2];
        if let Some(p) = previous {
            if current > p {
                sum += 1;
            }
        }
        previous = Some(current);
    }
    sum
}

pub fn read_input() -> anyhow::Result<Vec<usize>> {
    let f = File::open("input/1.txt")?;
    let reader = BufReader::new(f);
    reader
        .lines()
        .map(|l| {
            l.map_err(anyhow::Error::new)
                .and_then(|s| s.parse().map_err(anyhow::Error::new))
        })
        .collect::<anyhow::Result<Vec<_>, _>>()
}

pub fn run() -> anyhow::Result<()> {
    let input = read_input()?;

    let result_1 = calculate_1(&input);
    println!("{}", result_1);

    let result_2 = calculate_2(&input);
    println!("{}", result_2);

    Ok(())
}
